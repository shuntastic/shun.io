import { mat4 } from './../libs/gl-matrix';
class ShaderExercise {
  constructor(props) {
    console.log('loaded');
    this.startBtn = props.startEl ? document.querySelector(props.startEl) : document.body;

    this.canvas = document.createElement('canvas');
    this.canvas.setAttribute('id', 'ShaderExercise');
    this.canvas.classList.add('abs');
    this.canvas.classList.add('TL');
    this.canvas.classList.add('overlay');
    this.updateCanvas();

    // this.canvas.width = window.innerWidth;
    // this.cancas.height = window.innerHeight;
    this.gl = this.canvas.getContext('webgl');

    // If we don't have a GL context, give up now
    if (!this.gl) {
      alert('Unable to initialize Webthis.gl. Your browser or machine may not support it.');
      return;
    }

    /*
        shaders
    */

    // Vertex
    this.vsSource = `
        attribute vec4 aVertexPosition;
        attribute vec4 aVertexColor;
        uniform mat4 uModelViewMatrix;
        uniform mat4 uProjectionMatrix;
        varying lowp vec4 vColor;
        void main(void) {
            gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
            vColor = aVertexColor;
        }
        `;

    // Fragment

    this.fsSource = `
        varying lowp vec4 vColor;
        void main(void) {
            gl_FragColor = vColor;
        }
        `;

    this.shaderPrg = this.initShaderPrg(this.gl, this.vsSource, this.fsSource);

    this.programInfo = {
      program: this.shaderPrg,
      attribLocations: {
        vertexPosition: this.gl.getAttribLocation(this.shaderPrg, 'aVertexPosition'),
        vertexColor: this.gl.getAttribLocation(this.shaderPrg, 'aVertexColor')
      },
      uniformLocations: {
        projectionMatrix: this.gl.getUniformLocation(this.shaderPrg, 'uProjectionMatrix'),
        modelViewMatrix: this.gl.getUniformLocation(this.shaderPrg, 'uModelViewMatrix')
      }
    };
    window.addEventListener('resize',this.updateCanvas.bind(this));
    // this.startBtn.addEventListener('click', this.init.bind(this));
    this.init();
}
init() {
    console.log('inited');
    document.body.appendChild(this.canvas);
    this.buffers = this.initBuffers(this.gl);
    this.drawScene(this.gl, this.programInfo, this.buffers);
  }
  updateCanvas() {
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;

    this.WIDTH = window.innerWidth;
    this.HEIGHT = window.innerHeight;
  }

  initBuffers(gl) {
    const positionBuffer = gl.createBuffer();
    this.gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
    const positions = [1.0, 1.0, -1.0, 1.0, 1.0, -1.0, -1.0, -1.0];

    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);

    var colors = [
      1.0,
      1.0,
      1.0,
      1.0, // white
      1.0,
      0.0,
      0.0,
      1.0, // red
      0.0,
      1.0,
      0.0,
      1.0, // green
      0.0,
      0.0,
      1.0,
      1.0 // blue
    ];

    const colorBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);

    return {
      position: positionBuffer,
      color: colorBuffer
    };
  }

  drawScene(gl, programInfo, buffers) {
    gl.clearColor(0.0, 0.0, 0.0, 1.0); // Clear to black, fully opaque
    gl.clearDepth(1.0); // Clear everything
    gl.enable(gl.DEPTH_TEST); // Enable depth testing
    gl.depthFunc(gl.LEQUAL); // Near things obscure far things

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    const fieldOfView = (45 * Math.PI) / 180; // in radians
    const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
    const zNear = 0.1;
    const zFar = 100.0;
    const projectionMatrix = mat4.create();

    mat4.perspective(projectionMatrix, fieldOfView, aspect, zNear, zFar);

    // Set the drawing position to the "identity" point, which is
    // the center of the scene.
    const modelViewMatrix = mat4.create();

    // Now move the drawing position a bit to where we want to
    // start drawing the square.

    mat4.translate(
      modelViewMatrix, // destination matrix
      modelViewMatrix, // matrix to translate
      [-0.0, 0.0, -1.7]
    ); // amount to translate

    // Tell WebGL how to pull out the positions from the position
    // buffer into the vertexPosition attribute
    {
      const numComponents = 2;
      const type = gl.FLOAT;
      const normalize = false;
      const stride = 0;
      const offset = 0;
      gl.bindBuffer(gl.ARRAY_BUFFER, buffers.position);
      gl.vertexAttribPointer(
        programInfo.attribLocations.vertexPosition,
        numComponents,
        type,
        normalize,
        stride,
        offset
      );
      gl.enableVertexAttribArray(programInfo.attribLocations.vertexPosition);
    }

    // Tell WebGL how to pull out the colors from the color buffer
    // into the vertexColor attribute.
    {
      const numComponents = 4;
      const type = this.gl.FLOAT;
      const normalize = false;
      const stride = 0;
      const offset = 0;
      gl.bindBuffer(gl.ARRAY_BUFFER, buffers.color);
      gl.vertexAttribPointer(programInfo.attribLocations.vertexColor, numComponents, type, normalize, stride, offset);
      gl.enableVertexAttribArray(programInfo.attribLocations.vertexColor);
    }

    // Tell WebGL to use our program when drawing

    gl.useProgram(programInfo.program);

    // Set the shader uniforms

    gl.uniformMatrix4fv(programInfo.uniformLocations.projectionMatrix, false, projectionMatrix);
    gl.uniformMatrix4fv(programInfo.uniformLocations.modelViewMatrix, false, modelViewMatrix);

    {
      const offset = 0;
      const vertexCount = 4;
      this.gl.drawArrays(this.gl.TRIANGLE_STRIP, offset, vertexCount);
    }
  }

  /*
         shader program, so WebGL knows how to draw our data
    */

  initShaderPrg(gl, vsSource, fsSource) {
    const vertexShader = this.loadShader(gl, gl.VERTEX_SHADER, vsSource);
    const fragmentShader = this.loadShader(gl, gl.FRAGMENT_SHADER, fsSource);

    // Create the shader program

    const shaderPrg = gl.createProgram();
    gl.attachShader(shaderPrg, vertexShader);
    gl.attachShader(shaderPrg, fragmentShader);
    gl.linkProgram(shaderPrg);

    // If creating the shader program failed, alert
    if (!gl.getProgramParameter(shaderPrg, gl.LINK_STATUS)) {
      alert('Unable to initialize the shader program: ' + gl.getProgramInfoLog(shaderPrg));
      return null;
    }

    return shaderPrg;
  }

  loadShader(gl, type, source) {
    const shader = gl.createShader(type);

    // Send the source to the shader object
    gl.shaderSource(shader, source);
    // Compile the shader program

    gl.compileShader(shader);

    // See if it compiled successfully

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
      alert('Shader Error: ' + gl.getShaderInfoLog(shader));
      gl.deleteShader(shader);
      return null;
    }

    return shader;
  }
}

export default ShaderExercise;
