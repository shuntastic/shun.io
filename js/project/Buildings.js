import AudioHandler from '../utils/AudioHandler';
import * as PIXI from 'pixi.js';
import audioFile from '/assets/aud/Ballerina.mp3';
class Building {
  constructor(props) {
    //dims used for viz
    this.MAX = 110;
    this.INDEX = 0;
    this.startBtn = document.querySelector(props.startEl);
    // this.WIDTH = window.innerWidth - 40;
    // this.HEIGHT = this.WIDTH * 0.3623;
    this.WIDTH = window.innerWidth;
    this.HEIGHT = window.innerHeight;
    let type = 'WebGL';
    if (!PIXI.utils.isWebGLSupported()) {
      type = 'canvas';
    }

    this.imagePath = '../../assets/images/buildings/';
    this.imageData = [];
    this.imageOpts = [
      'Layer3.png',
      'Layer4.png',
      'Layer5.png',
      'Layer6.png',
      'Layer7.png',
      'Layer8.png',
      'Layer9.png',
      'Layer10.png',
      'Layer11.png',
      'Layer12.png',
      'Layer13.png',
      'Layer14.png',
      'Layer15.png',
      'Layer16.png',
      'Layer17.png',
      'Layer18.png',
      'Layer19.png',
      'Layer20.png',
      'Layer21.png',
      'Layer22.png',
      'Layer23.png',
      'Layer24.png',
      'Layer25.png',
      'Layer26.png',
      'Layer27.png',
      'Layer28.png',
      'Layer29.png',
      'Layer30.png',
      'Layer31.png',
      'Layer32.png',
      'Layer33.png',
      'Layer34.png',
      'Layer35.png',
      'Layer36.png',
      'Layer37.png',
      'Layer38.png',
      'Layer39.png',
      'Layer40.png',
      'Layer41.png',
      'Layer42.png',
      'Layer43.png',
      'Layer44.png',
      'Layer45.png',
      'Layer46.png',
      'Layer47.png',
      'Layer48.png'
    ];

    this.colorOpts = [
      { r: 255, g: 227, b: 255, hex: 0x518084 },
      { r: 178, g: 141, b: 178, hex: 0xd9afc7 },
      { r: 255, g: 243, b: 201, hex: 0x9e9660 },
      { r: 161, g: 204, b: 198, hex: 0x847f57 },
      { r: 150, g: 178, b: 174, hex: 0x3a5051 }
    ];
    this.cacheImages(this.imageOpts);

    for (var i = 0; i < this.MAX; i++) {
      this.imageData.push({
        running: false,
        isFirst: true
      });
    }
    const vals = {
      src: audioFile
    };
    this.audAlyzer = new AudioHandler(vals);

    this.startBtn.addEventListener('click', evt => {
      this.init();
    });
  }

  init() {
    this.audAlyzer.init();
    this.app = new PIXI.Application(this.WIDTH, this.HEIGHT, {
      transparent: true,
      antialias: true
    });

    PIXI.settings.RESOLUTION = window.devicePixelRatio;
    PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;

    document.body.appendChild(this.app.view);
    this.container = new PIXI.Container();
    this.app.stage.addChild(this.container);
    this.renderFrame();
  }

  cacheImages(imgArray) {
    const images = new Array();
    for (let i = 0; i < imgArray.length; i++) {
      images[i] = new Image();
      images[i].src = this.imagePath + imgArray[i];
    }
  }
  resetBuilding(ref) {
    let building = this.container.getChildByName('building_' + ref);
    let bldng = building.getChildByName('bldng_' + ref);
    let rectangle = building.getChildByName('rectangle_' + ref);
    bldng.height = this.audAlyzer.dataArray[ref] * (ref > 15 ? 1.25 : 0.35);
    bldng.width = bldng.height * Math.max(1.2, Math.random() * 2);
    bldng.alpha = 0.5;
    // bldng.blendMode = PIXI.BLEND_MODES.MULTIPLY;
    building.x = this.WIDTH + bldng.width - Math.random() * 10;
    building.y = this.HEIGHT - bldng.height;

    var colorIndex = this.colorOpts[
      Math.min(this.colorOpts.length - 1, Math.floor(Math.random() * this.colorOpts.length))
    ].hex;
    rectangle.beginFill(colorIndex);
    rectangle.drawRect(bldng.x, bldng.y, bldng.width, bldng.height);
    rectangle.endFill();
  }
  addBuilding(ref) {
    if (this.imageData[ref].isFirst) {
      let bldngRef = Math.floor(Math.random() * this.imageOpts.length);
      let building = new PIXI.Container();

      let bldng = PIXI.Sprite.from(this.imageOpts[bldngRef]);
      var rectangle = new PIXI.Graphics();
      building.name = 'building_' + ref;
      bldng.name = 'bldng_' + ref;
      rectangle.name = 'rectangle_' + ref;
      building.addChild(rectangle);
      building.addChild(bldng);
      this.container.addChild(building);
      this.resetBuilding(ref);

      this.app.ticker.add(this.buildingCheck.bind(this));

      this.imageData[ref].isFirst = false;
      this.imageData[ref].running = true;
    }
    // else {
    //   console.log('resetbuilding call',ref);
    //   this.resetBuilding(ref);
    // }
  }
  buildingCheck(evt) {
    console.log(evt);
    if (building.x < -(bldng.width + 5)) {
      console.log('screen reset', ref);
      // this.imageData[ref].running = false;
      this.resetBuilding(ref);
    } else {
      building.x -= bldng.width / 3;
    }
  }
  animateAudio() {
    this.WIDTH = window.innerWidth;
    this.HEIGHT = window.innerHeight;
    // this.WIDTH = window.innerWidth - 40;
    // this.HEIGHT = this.WIDTH * 0.3623;
    this.app.renderer.resize(this.WIDTH, this.HEIGHT);
    // HEIGHT = window.innerHeight;

    this.audAlyzer.analyser.getByteFrequencyData(this.audAlyzer.dataArray);

    // console.log('animateAudio',this.audAlyzer.getBufferLength());
    for (var i = 0; i < this.audAlyzer.bufferLength; i++) {
      if (this.audAlyzer.dataArray[i] > 220 && this.INDEX < this.MAX) {
        // console.log(i, this.audAlyzer.dataArray[i]);
        // if (!this.imageData[i].running) {
        this.addBuilding(this.INDEX);
        this.INDEX++;
        // }
      }
    }
  }

  renderFrame() {
    requestAnimationFrame(this.renderFrame.bind(this));
    this.animateAudio();
  }
}
export default Building;
