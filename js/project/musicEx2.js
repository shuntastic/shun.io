import AudioHandler from '../utils/AudioHandler';
// import audioFile from '/assets/aud/Equipoise.mp3';
import audioFile from '/assets/aud/Basquiat_Score_-_J_Ralph_(Radiant_Child_(2010)_Ending_Theme).mp3';
import logo_dot from '/assets/images/parts/logo_dot.svg';
import logo_h from '/assets/images/parts/logo_h.svg';
import logo_n from '/assets/images/parts/logo_n.svg';
import logo_s from '/assets/images/parts/logo_s.svg';
import logo_u from '/assets/images/parts/logo_u.svg';
import regeneratorRuntime from 'regenerator-runtime';
import * as THREE from 'three';

window.requestAnimationFrame = (function() {
  return (
    window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.msRequestAnimationFrame ||
    function(callback) {
      return window.setTimeout(callback, 0);
    }
  );
})();

window.cancelAnimationFrame = (function() {
  return (
    window.cancelAnimationFrame ||
    window.webkitCancelAnimationFrame ||
    window.mozCancelAnimationFrame ||
    window.msCancelAnimationFrame ||
    function(intervalKey) {
      window.clearTimeout(intervalKey);
    }
  );
})();

THREE.LegacySVGLoader = function(manager) {
  this.manager = manager !== undefined ? manager : THREE.DefaultLoadingManager;
};

THREE.LegacySVGLoader.prototype = {
  constructor: THREE.LegacySVGLoader,
  load: function(url, onLoad, onProgress, onError) {
    var scope = this;
    var parser = new DOMParser();
    var loader = new THREE.FileLoader(scope.manager);
    loader.load(
      url,
      function(svgString) {
        var doc = parser.parseFromString(svgString, 'image/svg+xml'); // application/xml
        onLoad(doc.documentElement);
      },
      onProgress,
      onError
    );
  }
};
class musicEx {
  constructor(props) {
    this.particleTotal = 300;
    this.startBtn = props.startEl ? document.querySelector(props.startEl) : document.body;
    this.startBtn.addEventListener('click', this.startAudio.bind(this));

    const vals = {
      src: props.audio ? props.audio : audioFile
    };
    this.audAlyzer = new AudioHandler(vals);

    this.canvas = this.createCanvas(false, 'musicEx', ['abs', 'TL', 'overlay']);
    this.pitchOutput = this.createCanvas(false, 'pitchOut', ['abs', 'BR']);
    this.updateCanvas();
    this.color = {};
    this.gradients = [
      {
        rotation: 315,
        colors: [0x8e160f, 0xd3aa4d]
      },

      {
        rotation: 135,
        colors: [0xfdbedf, 0x157fc2]
      },
      {
        rotation: 180,

        colors: [0xc9e013, 0x9c07d5]
      },

      {
        rotation: 135,
        colors: [0x8d4034, 0x7dfde4]
      },
      {
        rotation: 135,
        colors: [0xd1378b, 0x44c6f3]
      },
      {
        rotation: 180,
        colors: [0xc79a84, 0x44868d]
      },
      {
        rotation: 32,
        colors: [0x4adae9, 0xe2ccaf]
      },
      {
        rotation: 180,
        colors: [0x07dced, 0x912716]
      },

      {
        rotation: 180,
        colors: [0xdc8492, 0x0a7e6e]
      },
      {
        rotation: 225,
        colors: [0xe7a09c, 0x64a860]
      },
      {
        rotation: 135,
        colors: [0x1901b1, 0xf13dc1]
      },
      {
        rotation: 135,
        colors: [0x826c83, 0xf0f8b9]
      },
      {
        rotation: 135,
        colors: [0x1ac6fa, 0xbd8e97]
      },
      {
        rotation: 135,
        colors: [0x9e029f, 0x988c18]
      }
    ];
    this.objArray = [];
    this.siteRef = props.ref ? props.ref : Math.floor(Math.random() * this.gradients.length);

    // this.init();
  }

  async init() {
    window.scene = this.scene;
    document.querySelector('#main').appendChild(this.canvas);
    document.querySelector('#main').appendChild(this.pitchOutput);

    this.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
    this.renderer.setPixelRatio(window.devicePixelRatio ? window.devicePixelRatio : 1);
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderer.autoClear = false;
    this.renderer.setClearColor(0x000000, 0.0);
    this.canvas.appendChild(this.renderer.domElement);

    this.scene = new THREE.Scene();
    this.scene.fog = new THREE.Fog(0x000000, 1500, 2100);

    this.camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 100000);
    this.camera.position.z = 1500;
    this.scene.add(this.camera);

    this.particle = new THREE.Object3D();
    this.scene.add(this.particle);

    var geometry = new THREE.TetrahedronGeometry(2, 0);

    this.color.primary = this.gradients[this.siteRef].colors[0];
    this.color.secondary = this.gradients[this.siteRef].colors[1];

    this.material = new THREE.MeshPhongMaterial({
      color: 0xffffff,
      // color: this.color.primary,
      wireframe: false,
      flatShading: THREE.FlatShading
    });
    this.objS = await this.loadSVG(logo_s);
    this.objH = await this.loadSVG(logo_h);
    this.objU = await this.loadSVG(logo_u);
    this.objN = await this.loadSVG(logo_n);
    this.objDot = await this.loadSVG(logo_dot);

    this.objS.position.set(0.1, 0, 0);
    this.objH.position.set(0.2, 0, 0);
    this.objU.position.set(0.3, 0, 0);
    this.objN.position.set(0.4, 0, 0);
    this.objDot.position.set(0.5, 0, 0);
    this.objS.name = 'objS';
    this.objH.name = 'objH';
    this.objU.name = 'objU';
    this.objN.name = 'objN';
    this.objDot.name = 'objDot';

    this.scene.add(this.objS);
    this.scene.add(this.objH);
    this.scene.add(this.objU);
    this.scene.add(this.objN);
    this.scene.add(this.objDot);

    this.objArray = ['objS', 'objH', 'objU', 'objN', 'objDot'];
    this.ambientLight = new THREE.AmbientLight(0x999999);

    this.scene.add(this.ambientLight);

    this.lights = [];
    this.lights[0] = new THREE.DirectionalLight(0xffffff, 1);
    this.lights[0].position.set(1, 0, 0);
    this.lights[1] = new THREE.DirectionalLight(0x11e8bb, 1);
    this.lights[1].position.set(0.75, 1, 0.5);
    this.lights[2] = new THREE.DirectionalLight(0x8200c9, 1);
    this.lights[2].position.set(-0.75, -1, 0.5);
    this.scene.add(this.lights[0]);
    this.scene.add(this.lights[1]);
    this.scene.add(this.lights[2]);

    window.addEventListener('resize', this.onResize.bind(this), false);
  }
  startAudio(evt) {
    console.log('startaudio');
    this.init();
    this.audAlyzer.initAndStart();
    this.renderFrame();
    evt.target.removeEventListener('click', this.startAudio.bind(this));
    this.startBtn.addEventListener('click', this.pauseAudio.bind(this));
  }
  pauseAudio(evt) {
    console.log('pause');
    this.stopAnimation();
    this.audAlyzer.pause();
    evt.target.removeEventListener('click', this.pauseAudio.bind(this));
    this.startBtn.addEventListener('click', this.playAudio.bind(this));
  }
  playAudio(evt) {
    this.audAlyzer.play();
    this.renderFrame();
    evt.target.removeEventListener('click', this.playAudio.bind(this));
    this.startBtn.addEventListener('click', this.pauseAudio.bind(this));
  }
  stopAnimation() {
    cancelAnimationFrame(this.aniTrack);
  }
  renderFrame() {
    this.aniTrack = requestAnimationFrame(this.renderFrame.bind(this));
    this.animateAudio();
  }
  resetParticle(ref) {
    let mesh = this.particle.getObjectByName(`part${ref}`);

    mesh.position.set(Math.random() - 0.5, Math.random() - 0.5, Math.random() - 0.5).normalize();
    mesh.position.multiplyScalar(90 + Math.random() * 200);
    mesh.speed = Math.random() * 10;
  }
  animateAudio() {
    this.objArray.forEach((v, i) => {
      console.log(v, i);
      this.scene.getObjectByName(v).rotation.y += 0.1;
    });
    // this.audAlyzer.analyser.getByteFrequencyData(this.audAlyzer.dataArray);
    // for (var i = 0; i < this.audAlyzer.dataArray.length; i++) {
    //   console.log(this.objArray[ this.objArray.length % i ]);
    //   this.scene.getObjectByName( this.objArray[ this.objArray.length % i ] ).rotation.y += this.audAlyzer.dataArray[i] / 255;
    //   // part.intensity = Math.max(0.0000001, this.audAlyzer.dataArray[i] / 255);
    // }
    this.renderer.clear();
    this.renderer.render(this.scene, this.camera);
  }

  onResize() {
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(window.innerWidth, window.innerHeight);
  }

  // utils
  loadSVG(obj) {
    return new Promise((resolve, reject) => {
      var loader = new THREE.SVGLoader();
      loader.load(
        obj,
        function(evt) {
          var paths = evt.paths;
          var group = new THREE.Group();

          for (var i = 0; i < paths.length; i++) {
            var path = paths[i];
            var material = new THREE.MeshPhongMaterial({
              color: path.color,
              wireframe: false,
              shading: THREE.FlatShading,
              side: THREE.DoubleSide,
              depthWrite: false
            });
            var shapes = path.toShapes(true);

            for (var i = 0; i < shapes.length; i++) {
              var shape = shapes[i];
              var geometry = new THREE.ShapeBufferGeometry(shape);
              var mesh = new THREE.Mesh(geometry, material);
              group.add(mesh);
            }
          }
          resolve(group);
        },
        function(evt) {
          console.log((evt.loaded / evt.total) * 100 + '% loaded');
        },
        function(evt) {
          console.log(evt);
          reject();
        }
      );
    });
  }

  createCanvas(canvasFlag, id, classList) {
    const canvas = canvasFlag ? document.createElement('canvas') : document.createElement('div');
    canvas.setAttribute('id', id);
    if (typeof classList == Array) {
      canvas.classList.add.apply(classList);
    } else {
      canvas.classList.add(classList);
    }
    return canvas;
  }
  updateCanvas() {
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;
  }

  toDec(val) {
    return val / 255;
  }
  indexOfMax(arr) {
    if (arr.length === 0) {
      return -1;
    }
    var max = arr[0];
    var maxIdx = 0;
    for (var i = 1; i < arr.length; i++) {
      if (arr[i] > max) {
        maxIdx = i;
        max = arr[i];
      }
    }
    return maxIdx;
  }
}

export default musicEx;
