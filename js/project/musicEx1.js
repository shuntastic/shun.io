
import AudioHandler from '../utils/AudioHandler';
import audioFile from '/assets/aud/Basquiat_Score_-_J_Ralph_(Radiant_Child_(2010)_Ending_Theme).mp3';

window.requestAnimationFrame = (function() {
  return (
    window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.msRequestAnimationFrame ||
    function(callback) {
      return window.setTimeout(callback, 0);
    }
  );
})();

window.cancelAnimationFrame = (function() {
  return (
    window.cancelAnimationFrame ||
    window.webkitCancelAnimationFrame ||
    window.mozCancelAnimationFrame ||
    window.msCancelAnimationFrame ||
    function(intervalKey) {
      window.clearTimeout(intervalKey);
    }
  );
})();

class musicEx1 {
  constructor(props) {
    this.startBtn = props.startEl ? document.querySelector(props.startEl) : document.body;
    this.el = document.querySelector('#intro');
    // window.addEventListener('resize', this.updateCanvas.bind(this));
    this.startBtn.addEventListener('click', this.startAudio.bind(this));
    
    // document.querySelector('#main').appendChild(this.canvas);
    
    const vals = {
        src: audioFile
    };
    this.audAlyzer = new AudioHandler(vals);
    
    this.init();
}

init() {
    /* */
}
startAudio(evt) {
    this.audAlyzer.initAndStart();
    this.renderFrame();
    evt.target.removeEventListener('click', this.startAudio.bind(this));
    evt.target.addEventListener('click', this.pauseAudio.bind(this));
  }
  pauseAudio(evt){
      this.stopAnimation();
    this.audAlyzer.pause();
    evt.target.removeEventListener('click', this.pauseAudio.bind(this));
    evt.target.addEventListener('click', this.playAudio.bind(this));

  }
  playAudio(evt){
    this.audAlyzer.play();
    this.renderFrame();
    evt.target.removeEventListener('click', this.playAudio.bind(this));
    evt.target.addEventListener('click', this.pauseAudio.bind(this));

  }
  stopAnimation(){
        cancelAnimationFrame(this.aniTrack);
  }
  renderFrame() {
    this.aniTrack = requestAnimationFrame(this.renderFrame.bind(this));
    this.animateAudio();
  }
  animateAudio() {
    var array = new Uint8Array(this.audAlyzer.analyser.fftSize);
    this.audAlyzer.analyser.getByteTimeDomainData(array);
    var average = 0;
    var max = 0;
    for (var a of array) {
        a = Math.abs(a - 128);
        average += a;
        max = Math.max(max, a);
    }
    average /= array.length;
    // this.el.style.opacity = parseFloat(0.95+(average*.00165));
    this.el.style.transform = 'rotateZ('+parseFloat(average*.0165)+'deg)';
  }


  // utils
  toDec(val) {
    return val / 255;
  }
  indexOfMax(arr) {
    if (arr.length === 0) {
      return -1;
    }
    var max = arr[0];
    var maxIdx = 0;
    for (var i = 1; i < arr.length; i++) {
      if (arr[i] > max) {
        maxIdx = i;
        max = arr[i];
      }
    }
    return maxIdx;
  }
}

export default musicEx1;
