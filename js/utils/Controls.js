import './../../assets/images/controls/apple.svg';
import './../../assets/images/controls/facebook.svg';
import './../../assets/images/controls/info.svg';
import './../../assets/images/controls/instagram.svg';
import './../../assets/images/controls/link.svg';
import './../../assets/images/controls/pause.svg';
import './../../assets/images/controls/play.svg';
import './../../assets/images/controls/replay.svg';
import './../../assets/images/controls/soundcloud.svg';
import './../../assets/images/controls/spotify.svg';
import './../../assets/images/controls/tidal.svg';
import './../../assets/images/controls/twitter.svg';
import './../../assets/images/controls/volume_off.svg';
import './../../assets/images/controls/volume_on.svg';
// import AudioHandler from '../utils/AudioHandler';

class Controls {
  constructor(props) {
    this.controls = {};
    this.autoplay = props.autoplay !== undefined ? props.autoplay : true;
    this.path = props.path !== undefined ? props.path : './../../assets/images/controls/';
    this.options = [
      'play',
      'pause',
      'info',
      'apple',
      'facebook',
      'instagram',
      'link',
      'replay',
      'soundcloud',
      'spotify',
      'tidal',
      'twitter',
      'volume_off',
      'volume_on'
    ];
    this.audioCheck = typeof props.play.src !== undefined;
    this.options.forEach((option)=>{

      if (typeof props[option] !== undefined) {

        let control = {};
        control.el = document.createElement('a');
        control.el.id = option;
        switch(option) {
          case 'pause':
          case 'play':
            if(typeof props[option].src !== undefined){
              this.audioCheck = true;
              this.audio = new Audio(props[option].src);
              this.audio.addEventListener('onloaded', this.onLoaded.bind(this));
              this.audio.load();
              // this.src = null;
          
            }
          case 'replay':
          case 'volume_off':
          case 'volume_on':

          if (typeof props[option] == object) {
            
          }
          if(typeof props[option].onclick !== undefined){
            control.el.addEventListener('click',props[option].onclick);
          } else if(this.audioCheck) {
            control.el.addEventListener('click',this.onclick[option].bind(this));
          }
          break;
          
          default:

            if (typeof props[option] == string) {
              control.el.setAttribute('href',props[option]);
              control.el.setAttribute('target','_blank');
            }
          
          break;
        }
        control.el.innerHTML = `<img src="${this.path}${option}.svg" />`;
        this.controls.push(control);
      }
    })
    
    this.analyser = null;
    this.bufferLength = null;
    this.dataArray = null;
  }
  onLoaded(evt) {
    console.log('onloaded');
    if(this.autoplay)
    this.playAudio();
    /* */
  }
  playAudio(){
    this.audio.play();
  }
  pauseAudio(){
    this.audio.pause();
  }
  init() {
    /* */
  }

  play(){
    /* */
  }

}
export default Controls;
